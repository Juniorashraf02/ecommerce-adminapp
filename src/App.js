import { Switch, Route } from "react-router-dom";
import Home from "./components/Home/index";
import Signin from "./components/signin/index";
import Signup from "./components/signup/index";
import { PrivateRoute } from "./components/HOC/PrivateRoute";
import { useEffect } from "react";
import { isUserLoggedIn } from "./redux/actions/auth.action";
import { useDispatch, useSelector } from "react-redux";
import Orders from './components/Orders/index';
import Products from './components/Products/index';
import Category from './components/category/index';

function App() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  useEffect(() => {
    if (!auth.authenticate) {
      dispatch(isUserLoggedIn());
    }
  }, []);

  return (
    <div className="App">
      <Switch>
        <PrivateRoute path="/" exact component={Home} />
        <PrivateRoute path="/orders" component={Orders} />
        <PrivateRoute path="/products" component={Products} />
        <PrivateRoute path="/category" component={Category} />
        <Route path="/signin" component={Signin}></Route>
        <Route path="/signup" component={Signup}></Route>
      </Switch>
    </div>
  );
}

export default App;
