import { actionType } from "../actions/actionTypes";

const initialState = {
  token: null,
  user: {
    firstName: "",
    lastName: "",
    email: "",
    picture: "",
  },
  authenticate: false,
  authenticatin: false,
  loading: false,
  error: null,
  message: "",
};

export default (state = initialState, action) => {
  console.log(action);

  switch (action.type) {
    case actionType.LOGIN_REQUEST:
      state = {
        ...state,
        authenticatin: true,
      };
      break;

    case actionType.LOGIN_SUCCESS:
      state = {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        authenticate: true,
        authenticating: false,
      };
      break;
      case actionType.LOGOUT_REQUEST: 
      state = {
        ...state,
        loading: true,
      }
      break;
      case actionType.LOGOUT_SUCCESS: 
      state = {
        ...initialState,
      }
      break;
      case actionType.LOGOUT_ERROR: 
      state = {
        ...state,
        error: action.payload.error,
        loading: false, 
      }
      break;
  }

   return state;
};
