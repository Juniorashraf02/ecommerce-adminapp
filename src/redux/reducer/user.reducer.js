import { signupAction } from "./../actions/actionTypes";

const initialState = {
  loading: false,
  message: "",
  error: null,
};

export default (state = initialState, action) => {
    console.log(action);


  switch (action.type) {
    case signupAction.USER_REGISTER_REQUEST:
      state = {
        ...state,
        loading: true,
      };
      break;

    case signupAction.USER_REGISTER_REQUEST_SUCCESS:
      state = {
        ...state,
        loading: false,
        message: action.payload.message,
      };
      break;

    case signupAction.USER_REGISTER_REQUEST_ERROR:
      state = {
        ...state,
        loading: false,
        error: action.payload.error,
      };
      break;

    }
    return state;
};
