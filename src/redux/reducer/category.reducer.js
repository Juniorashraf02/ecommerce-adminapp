import { categoryActions } from "../actions/actionTypes";

const initialState = {
  categories: [],
  loading: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case categoryActions.GET_ALL_CATEGORIES_SUCCESS:
      state = {
        ...state,
        categories: action.payload.categories,
      }
      break;

      case categoryActions.ADD_CATEGORIES_REQUEST:
        state = {
            ...state,
            loading: true,
        }
        break;

        case categoryActions.ADD_CATEGORIES_SUCCESS:
            state= {
                ...state,
                loading: false,
                error: false,
            }

        case categoryActions.ADD_CATEGORIES_FAILURE:
            state = {
                ...initialState,
            }
  }
  return state;
};
