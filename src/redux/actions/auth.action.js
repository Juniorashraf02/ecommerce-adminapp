import { actionType } from "./actionTypes";
import { axiosInstance } from "./../../helpers/axios";

export const login = (user) => {
  console.log(user);

  return async (dispatch) => {
    dispatch({ type: actionType.LOGIN_REQUEST });

    const res = await axiosInstance.post("/admin/signin", {
      ...user,
    });

    if (res.status === 200) {
      const { token, user } = res.data;
      localStorage.setItem("token", token);

      localStorage.setItem("user", JSON.stringify(user));

      dispatch({
        type: actionType.LOGIN_SUCCESS,
        payload: {
          token,
          user,
        },
      });
    } else {
      if (res.status === 400) {
        dispatch({
          type: actionType.LOGIN_ERROR,
          payload: {
            error: res.data.error,
          },
        });
      }
    }
  };
};

export const isUserLoggedIn = () => {
  return async (dispatch) => {
    const token = localStorage.getItem("token");
    const user = JSON.parse(localStorage.getItem("user"));
    if (token) {
      dispatch({
        type: actionType.LOGIN_SUCCESS,
        payload: {
          token,
          user,
        },
      });
    } else {
      dispatch({
        type: actionType.LOGIN_ERROR,
        payload: {
          error: "failed to log in",
        },
      });
    }
  };
};

export const signOut = () => {
  return async (dispatch) => {
    dispatch({ type: actionType.LOGOUT_REQUEST });
    const res = await axiosInstance.post("/admin/signout");

    if (res.status === 200) {
      localStorage.clear();
      dispatch({
        type: actionType.LOGOUT_SUCCESS,
        payload: { message: res.data.message },
      });
    } else {
      dispatch({
        type: actionType.LOGOUT_ERROR,
        payload: { error: res.data.error },
      });
    }
  };
};
