import { actionType, signupAction } from "./actionTypes";
import { axiosInstance } from "./../../helpers/axios";


export const signup = (user) => {
    console.log(user);
  
    return async (dispatch) => {
      dispatch({ type: signupAction.USER_REGISTER_REQUEST });
  
      const res = await axiosInstance.post("/admin/signup", {
        ...user,
      });
  
      if (res.status === 200) {
        const { message } = res.data;

  
        dispatch({
          type: signupAction.USER_REGISTER_REQUEST_SUCCESS,
          payload: {
            message
          },
        });
      } else {
        if (res.status === 400) {
          dispatch({
            type: signupAction.USER_REGISTER_REQUEST_ERROR,
            payload: {
              error: res.data.error,
            },
          });
        }
      }
    };
  };