
import { axiosInstance } from './../../helpers/axios';
import { categoryActions } from './actionTypes';




export const getAllCategory = () => {
  return async (dispatch) => {
    dispatch({type: categoryActions.GET_ALL_CATEGORIES_REQUEST})
    const res = await axiosInstance.get('/category/get-category');
    console.log(res);

    const {categoryList}= res.data;
    if(res.status===200){
      dispatch({
        type: categoryActions.GET_ALL_CATEGORIES_SUCCESS,
        payload: {categories: categoryList}
       });
    } else{
      dispatch({
        type: categoryActions.GET_ALL_CATEGORIES_FAILURE,
        payload: {error: res.data.error}
      });
    }
  };
};


export const addCategory = (form) => {
  return async (dispatch)=>{
    dispatch({type: categoryActions.ADD_CATEGORIES_REQUEST})
    const res = await axiosInstance.post('/category/create', form);
    if(res.status===200){
      dispatch({type: categoryActions.ADD_CATEGORIES_SUCCESS,
      payload: res.data.category
      })
    } else {
      dispatch({
        type: categoryActions.ADD_CATEGORIES_FAILURE,
        error: res.data.error
      })
    }
  }
}
