import React from 'react'
import Header from './../Navbar/Header';
import { Col } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import './index.css'

 const Layout =(props)=> {
  return (
    <>
    <Header />
    {
      props.sidebar ?
      <Container fluid>
       <Row>
         <Col md={2} className="sidebar ">
           <ul>
           {/* <Nav.Link href="/">Home</Nav.Link>
           <Nav.Link href="page">Page</Nav.Link>
           <Nav.Link href="category">Category</Nav.Link>
           <Nav.Link href="products">Products</Nav.Link>
           <Nav.Link href="orders">Orders</Nav.Link> */}
             <li><NavLink exact to={`/`}>Home</NavLink></li>
             <li><NavLink to={`/page`}>Page</NavLink></li>
             <li><NavLink to={`/category`}>Category</NavLink></li>
             <li><NavLink to={`/products`}>Products</NavLink></li>
             <li><NavLink to={`/orders`}>Orders</NavLink></li>
           </ul>
         </Col>
         <Col md={10} style={{ marginLeft: 'auto', paddingTop: '60px' }}>
           {props.children}
         </Col>
       </Row>
     </Container>
     :
     props.children
    }
     
     
 </>
  )
}
export default Layout;
