import React, { useState } from "react";
import Layout from "./../Layout/Layout";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCategory, getAllCategory } from "../../redux/actions";
import { Button, Container, Modal, Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import Input from "./../UI/Input";

const Category = (props) => {
  const category = useSelector((state) => state.category);
  const dispatch = useDispatch();

  const [show, setShow] = useState(false);
  

  const [categoryName, setCategoryName] = useState();
  const [parentId, setParentId] = useState();
  const [categoryImg, setCategoryImg] = useState();

  useEffect(() => {
    dispatch(getAllCategory());
  }, []);


  const handleShow = () => setShow(true);
  const handleClose = () => {
    const form = new FormData();


    form.append('name', categoryName);
    form.append('parentId', parentId);
    form.append('categoryImg', categoryImg);
    dispatch(addCategory(form))
    // const cat = {
    //     categoryName,
    //     parentId,
    //     categoryImage
    // }
    // console.log(cat)
    setShow(false)
  };

 

  const renderCategories = (categories) => {
    let myCategories = [];
    for (let category of categories) {
      myCategories.push(
        <li key={category.name}>
          {category.name}
          {category.children.length > 0 ? (
            <ul>{renderCategories(category.children)}</ul>
          ) : null}
        </li>
      );
    }

    return myCategories;
  };

  const createCategoryList = (categories, options = []) => {
    for (let category of categories) {
      options.push({ value: category._id, name: category.name });
      if (category.children.length > 0) {
        createCategoryList(category.children, options);
      }
    }
    return options;
  };

  const handleCategoryImage = (e) => {
    setCategoryImg(e.target.files[0]);
  };

  return (
    <Layout sidebar>
      <Container>
        <Row>
          <Col>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <h3>Category</h3>

              {/* <!-- Button trigger modal --> */}
              <Button variant="primary" onClick={handleShow}>
                ADD
              </Button>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <ul>{renderCategories(category.categories)}</ul>
          </Col>
        </Row>
      </Container>

      {/* Modal  */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Set new category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Input
            value={categoryName}
            placeholder="category name"
            onChange={(e) => setCategoryName(e.target.value)}
          />

          <select
            className="form-control mt-2"
            value={parentId}
            onChange={(e) => setParentId(e.target.value)}
          >
            <option>Select category</option>
            {createCategoryList(category.categories).map((option) => (
              <option key={category._id} value={option.value}>
                {option.name}
              </option>
            ))}
          </select>

          <input className="mt-2"
            type="file"
            name="categoryImage"
            onChange={handleCategoryImage}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Layout>
  );
};

export default Category;
