import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useSelector } from "react-redux";
import { signOut } from "./../../redux/actions/auth.action";
import { useDispatch } from "react-redux";

function Header() {
  const dispatch = useDispatch();
  const userSignout = (e) =>{
    e.preventDefault();
    dispatch(signOut())
  }

  const rederLoggedInLinks = () => {
 
    return (
      <Nav>
        <Nav.Link eventKey={2} href="/" onClick={userSignout}>
          sign out
        </Nav.Link>
      </Nav>
    );
  };

  const renderNonLoggedInLinks = () => {
    return (
      <Nav>
        <Nav.Link href="signin">sign in</Nav.Link>

        <Nav.Link eventKey={2} href="signup">
          sign up
        </Nav.Link>
      </Nav>
    );
  };

  const auth = useSelector((state) => state.auth);

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Admin Dashboard</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">{/*this is for gap  */}</Nav>
          {auth.authenticate ? rederLoggedInLinks() : renderNonLoggedInLinks()}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;
